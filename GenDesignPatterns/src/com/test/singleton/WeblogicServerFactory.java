package com.test.singleton;

public class WeblogicServerFactory extends ServerFactory {

	private static ServerFactory serverFactory= new WeblogicServerFactory();
	
	@Override
	public Server getServer(String type) {
		if(type.equalsIgnoreCase("wls8")){
			return new WLS8();
		}
		else {
			return new WLS10();
		}
		
	}

	public static ServerFactory getFactoryInstance() {
		return serverFactory;
	}
	
	

}
