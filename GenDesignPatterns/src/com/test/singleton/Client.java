package com.test.singleton;

public class Client {
	
	
	public static void main(String[] args) {
		
		/*ServerFactory sf =  new WeblogicServerFactory();
		sf.getServer("wls8").getHome();*/
		
		ServerFactory sfsingleton = WeblogicServerFactory.getFactoryInstance();
		sfsingleton.getServer("wls8").getHome();
		
	}

}
