package com.test.policy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class PolicyInvocator implements InvocationHandler{
	
	private Object realSubject;

	public PolicyInvocator(Object realSubject){
		this.realSubject = realSubject;
	}

	@Override
	public Object invoke(Object paramObject, Method paramMethod,
			Object[] paramArrayOfObject) throws Throwable {
		
		Object result = null;
		try {

			result = paramMethod.invoke(realSubject, paramArrayOfObject);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
