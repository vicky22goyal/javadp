package com.test.policy;

public interface Policy {
	
	
	
	String getName(String userId);
	
	String getPolicy(String name);

}
