package com.test.policy;

public class Annuity implements Policy {

	@Override
	public String getName(String userId) {
		System.out.println("In getname Annuity");
		return "Vicky";
	}

	@Override
	public String getPolicy(String name) {
		System.out.println("In getPolicy Annuity");
		return "Term Plan";
	}

}
