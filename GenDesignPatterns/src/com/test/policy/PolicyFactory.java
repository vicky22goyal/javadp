package com.test.policy;

public class PolicyFactory {
	
	private static PolicyFactory policyFactory = new PolicyFactory();
	
	public static PolicyFactory getInstance (){
		return policyFactory;
	}
	
	
	public Policy getTypeOfPolicy(String type) {
		
		if(type.equalsIgnoreCase("annuity")){
			return new Annuity();
		}
		if(type.equalsIgnoreCase("individual")){
			return new Individual();
		}
		return null;
	
	}
	
}
