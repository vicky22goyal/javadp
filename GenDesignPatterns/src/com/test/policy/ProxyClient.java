package com.test.policy;

import java.lang.reflect.Proxy;

public class ProxyClient {
	
	// use of Singleton, Factory Pattern and proxy Pattern
	public static void main(String[] args) {
		PolicyFactory factory = PolicyFactory.getInstance();
		Policy policy = factory.getTypeOfPolicy("annuity");
		
		Policy proxy = (Policy) Proxy.newProxyInstance(policy.getClass()
				.getClassLoader(), policy.getClass().getInterfaces(),
				new PolicyInvocator(policy));
		System.out.println(proxy.getPolicy("test"));
		// now proxy.getPolicy will give a call to Invocator's invoke method
	}

}
