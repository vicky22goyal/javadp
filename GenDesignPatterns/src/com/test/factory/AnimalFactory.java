package com.test.factory;

public class AnimalFactory {
	
	public Animal createAnimal(String str){
		
		if(str.equals("dog")){
		    return new Dog();
		    }
		else if (str.equals("horse")){
		    return new Horse();
		    }
		
		else if(str.equals("lion")){
			return new Lion();
			}
		else{
			return new Animal();
		}
	}

}
