package com.test.builder;

public class Client {
	public static void main(String[] args) {
		HouseBuilder builder = new Mansion();
		CivilEngineer engineer = new CivilEngineer(builder);
		engineer.constructHouse();

		System.out.println("Builder constructed: "+engineer.getHouse());
	}
}