package com.test.builder;

public class Mansion implements HouseBuilder {

	private House house;

	public Mansion() {
		this.house = new House();
	}

	public void buildBasement() {
		house.setBasement("Awesome Basement");
	}

	public void buildStructure() {
		house.setStructure("Large Structure");
	}

	public void buildInterior() {
		house.setInterior("Eye Candy Interiors");
	}

	public void bulidRoof() {
		house.setRoof("Large Roof");
	}

	public House getHouse() {
		return this.house;
	}
}