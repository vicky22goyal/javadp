package com.test.builder;

public class MicroHouse implements HouseBuilder {
	private House house;

	public MicroHouse() {
		this.house = new House();
	}

	public void buildBasement() {
		house.setBasement("Small basement");
	}

	public void buildStructure() {
		house.setStructure("Simple Structure");
	}

	public void buildInterior() {
		house.setInterior("Bad interiors");
	}

	public void bulidRoof() {
		house.setRoof("Small Roof");
	}

	public House getHouse() {
		return this.house;
	}

}
